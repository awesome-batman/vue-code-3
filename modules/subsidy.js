
import axios from 'axios';

import {
    GET_SUBSIDY_QUESTIONS_REQUEST,
    GET_SUBSIDIES_REQUEST,
    GET_SUBSIDY_BEMA_REQUEST,
    GET_SUBSIDY_GOZ_REQUEST,
    GET_SUBSIDY_PRODUCT_GROUPS_REQUEST,

    SET_QUESTIONS,
    SET_SUBSIDIES,
    SET_SUBSIDY_BEMA,
    SET_SUBSIDY_GOZ,
    SET_SUBSIDY_PRODUCT_GROUPS,

    SAVE_CALCULATION_ACTION,
    SAVE_CALCULATION_COMMIT,
} from "../mutation/subsidy";

/**
 * @type {{}}
 */
const getters = {};

const actions = {
    /**
     * @param commit
     * @param state
     * @param options = [
     *   {
     *       toothNumber: 0,
     *       option: 'ab',
     *   }
     * ]
     */
    [GET_SUBSIDY_QUESTIONS_REQUEST]: function ({commit, state}, { options = [] } = {}) {
        return new Promise((resolve, reject) => {
            if ('questions' in state.questionsList && Object.keys(state.questionsList.questions).length) {
                resolve(state.questionsList);
            } else {
                axios.post(`/api/subsidy/get-questions`, {
                    options,
                }).then(response => {
                    if (response && response.data && response.data.list) {
                        commit(SET_QUESTIONS, response.data.list);
                        resolve(response.data.list);
                    } else {
                        reject(`Invalid Response from ${GET_SUBSIDY_QUESTIONS_REQUEST}`);
                    }
                }).catch(error => {
                    reject(error);
                });
            }
        });
    },

    /**
     * @param commit
     * @param state
     * @returns {Promise<any>}
     */
    [GET_SUBSIDY_GOZ_REQUEST]: function ({ commit, state }) {
        return new Promise((resolve, reject) => {
            if (state.gozList.length) {
                resolve(state.gozList);
            } else {
                axios.get(`/api/subsidy/get-goz`).then(response => {
                    if (response && response.data && response.data.list) {
                        commit(SET_SUBSIDY_GOZ, response.data.list);
                        resolve(response.data.list);
                    } else {
                        reject(`Invalid Response from ${GET_SUBSIDY_GOZ_REQUEST}`);
                    }
                }).catch(error => {
                    reject(error);
                });
            }
        });
    },

    /**
     * @param commit
     * @param state
     * @returns {Promise<any>}
     */
    [GET_SUBSIDY_PRODUCT_GROUPS_REQUEST]: function ({ commit, state }) {
        return new Promise((resolve, reject) => {
            if (Object.keys(state.productGroups).length) {
                resolve(state.productGroups);
            } else {
                axios.get(`/api/subsidy/get-product-groups`).then(response => {
                    if (response && response.data && response.data.list) {
                        commit(SET_SUBSIDY_PRODUCT_GROUPS, response.data.list);
                        resolve(response.data.list);
                    } else {
                        reject(`Invalid Response from ${GET_SUBSIDY_PRODUCT_GROUPS_REQUEST}`);
                    }
                }).catch(error => {
                    reject(error);
                });
            }
        });
    },

    /**
     * @param commit
     * @param state
     * @returns {Promise<any>}
     */
    [GET_SUBSIDY_BEMA_REQUEST]: function ({ commit, state }) {
        return new Promise((resolve, reject) => {
            if (state.bemaList.length) {
                resolve(state.bemaList);
            } else {
                axios.get(`/api/subsidy/get-bema`).then(response => {
                    if (response && response.data && response.data.list) {
                        commit(SET_SUBSIDY_BEMA, response.data.list);
                        resolve(response.data.list);
                    } else {
                        reject(`Invalid Response from ${GET_SUBSIDY_BEMA_REQUEST}`);
                    }
                }).catch(error => {
                    reject(error);
                });
            }
        });
    },

    /**
     * @param commit
     * @param state
     * @returns {Promise<any>}
     */
    [GET_SUBSIDIES_REQUEST]: function ({ commit, state }) {
        return new Promise((resolve, reject) => {
            if (state.subsidiesList.length) {
                resolve(state.subsidiesList);
            } else {
                axios.get(`/api/subsidy/get-subsidies`).then(response => {
                    if (response && response.data && response.data.list) {
                        commit(SET_SUBSIDIES, response.data.list);
                        resolve(response.data.list);
                    } else {
                        reject(`Invalid Response from ${GET_SUBSIDIES_REQUEST}`);
                    }
                }).catch(error => {
                    reject(error);
                });
            }
        });
    },

    /**
     * @param commit
     * @param state
     * @param data
     */
    [SAVE_CALCULATION_ACTION]: function({ commit, state }, data) {
        commit(SAVE_CALCULATION_COMMIT, data);
    },
};

/**
 * @type {{}}
 */
const mutations = {
    [SET_QUESTIONS]: function (store, list) {
        store.questionsList = list;
    },

    [SET_SUBSIDIES]: function (store, list) {
        store.subsidiesList = list;
    },

    [SET_SUBSIDY_BEMA]: function (store, list) {
        store.bemaList = list;
    },

    [SET_SUBSIDY_GOZ]: function (store, list) {
        store.gozList = list;
    },

    [SET_SUBSIDY_PRODUCT_GROUPS]: function (store, list) {
        store.productGroups = list;
    },

    [SAVE_CALCULATION_COMMIT]: function (store, data) {
        store.savedCalculations = {...store.savedCalculations, ...data};
    },
};

/**
 * @returns {{questionsList: {}, gozList: Array, bemaList: Array, subsidiesList: Array, savedCalculations: {}, productGroups: {}}}
 */
const state = function() {
    return {
        /**
         * Example Output:
         *
         * {
         *    'Question': [{}],
         *
         */
        questionsList: {},

        /**
         * Example Output:
         *
         * [
         *   {
         *     description: 0,
         *     min_factor: 0,
         *     max_factor: 0,
         *     standard_factor: 0,
         *     slug: '',
         *     money_points: '0',
         *   }
         * ],
         *
         */
        gozList: [],

        /**
         * Example Output:
         *
         * [
         *   {
         *     slug: '',
         *     money_points: '0',
         *     description: '',
         *   }
         * ],
         *
         */
        bemaList: [],

        /**
         * Example Output:
         *
         * [
         *   {
         *     number: '',
         *     bonuses: [],
         *   }
         * ],
         *
         */
        subsidiesList: [],

        /**
         * {
         *   toothNumber: {
         *       goz: {
         *           goz_number: cost,
         *       },
         *       bema: [
         *           bema_number,
         *       ],
         *       materials: {
         *         materialName: {
         *             cost: 0,
         *             quantity: 0,
         *             group: ''
         *         }
         *       },
         *       selectedOptions: [option],
         *   }
         * }
         */
        savedCalculations: {},

        /**
         * {
         *   groupSlug: {
         *   product:  {
         *       'id': 0,
         *       'preselected_quantity': 0,
         *       'name': 'test'
         *       'cost': 0,
         *   },
         *   quantities:[],
         *   description: ''
         *   }
         * }
         * }
         */
        productGroups: {},
    }
};

/**
 * @type {string}
 */
export const SUBSIDY_MODULE_NAME = 'SUBSIDY';

/**
 * @type {{namespaced: boolean, getters: {}, actions: {}, mutations: {}, state: (function(): {questionsList: {}, gozList: Array, bemaList: Array, subsidiesList: Array, savedCalculations: {}, productGroups: {}})}}
 */
export const subsidy = {
    namespaced: true,
    getters: getters,
    actions: actions,
    mutations: mutations,
    state: state,
};
