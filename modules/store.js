import vue from 'vue';
import vuex from 'vuex';
import { subsidy, SUBSIDY_MODULE_NAME} from "./modules/subsidy";

vue.use(vuex);

export default new vuex.Store({
    namespaced: true,
    modules: {
        [SUBSIDY_MODULE_NAME]: subsidy,
    },
});
